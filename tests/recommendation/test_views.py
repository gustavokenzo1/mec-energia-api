import unittest
from datetime import date, timedelta
from recommendation.views import RecommendationViewSet
from unittest.mock import Mock, patch
from rest_framework.request import Request
from django.test import RequestFactory

class TestRecommendationViewSet(unittest.TestCase):
    def setUp(self):
        self.viewset = RecommendationViewSet()
        self.blue = Mock()
        self.green = Mock()
        self.blue.end_date = date.today()
        self.green.end_date = date.today()
        self.factory = RequestFactory()

    @patch('universities.models.ConsumerUnit.objects.get')
    def test_blue_end_date_greater_than_today(self, mock_get):
        mock_get.return_value = Mock()
        mock_request = self.factory.get('/')
        pk = 'algumId'

        self.blue.end_date = date.today() + timedelta(days=1)
        self.viewset._get_tariffs = lambda x, y: (self.blue, self.green)

        mock_get.return_value.get_energy_bills_for_recommendation.return_value = []
        mock_get.return_value.get_energy_bills_pending.return_value = []

        response = self.viewset.retrieve(mock_request, pk)
        self.assertIn('Atualize as tarifas vencidas para aumentar a precisão da análise', response.data['warnings'])

    @patch('universities.models.ConsumerUnit.objects.get')
    def test_green_end_date_greater_than_today(self, mock_get):
        mock_get.return_value = Mock()
        mock_request = self.factory.get('/')
        pk = 'algumId'

        self.green.end_date = date.today() + timedelta(days=1)
        self.viewset._get_tariffs = lambda x, y: (self.blue, self.green)

        mock_get.return_value.get_energy_bills_for_recommendation.return_value = []
        mock_get.return_value.get_energy_bills_pending.return_value = []

        response = self.viewset.retrieve(mock_request, pk)
        self.assertIn('Atualize as tarifas vencidas para aumentar a precisão da análise', response.data['warnings'])

    @patch('universities.models.ConsumerUnit.objects.get')
    def test_blue_end_date_equal_today(self, mock_get):
        mock_get.return_value = Mock()
        mock_request = self.factory.get('/')
        pk = 'algumId'

        self.blue.end_date = date.today()
        self.viewset._get_tariffs = lambda x, y: (self.blue, self.green)

        mock_get.return_value.get_energy_bills_for_recommendation.return_value = []
        mock_get.return_value.get_energy_bills_pending.return_value = []

        response = self.viewset.retrieve(mock_request, pk)
        self.assertIn('Atualize as tarifas vencidas para aumentar a precisão da análise', response.data['warnings'])

    @patch('universities.models.ConsumerUnit.objects.get')
    def test_green_end_date_equal_today(self, mock_get):
        mock_get.return_value = Mock()
        mock_request = self.factory.get('/')
        pk = 'algumId'

        self.green.end_date = date.today()
        self.viewset._get_tariffs = lambda x, y: (self.blue, self.green)

        mock_get.return_value.get_energy_bills_for_recommendation.return_value = []
        mock_get.return_value.get_energy_bills_pending.return_value = []

        response = self.viewset.retrieve(mock_request, pk)
        self.assertIn('Atualize as tarifas vencidas para aumentar a precisão da análise', response.data['warnings'])

    @patch('universities.models.ConsumerUnit.objects.get')
    def test_blue_end_date_less_than_today(self, mock_get):
        mock_get.return_value = Mock()
        mock_request = self.factory.get('/')
        pk = 'algumId'

        self.blue.end_date = date.today() - timedelta(days=1)
        self.viewset._get_tariffs = lambda x, y: (self.blue, self.green)

        mock_get.return_value.get_energy_bills_for_recommendation.return_value = []
        mock_get.return_value.get_energy_bills_pending.return_value = []

        response = self.viewset.retrieve(mock_request, pk)
        self.assertNotIn('Atualize as tarifas vencidas para aumentar a precisão da análise', 'Lance todas as faturas dos últimos 12 meses para aumentar a precisão da análise. Foram lançadas apenas 0 faturas', response.data['warnings'])

    @patch('universities.models.ConsumerUnit.objects.get')
    def test_green_end_date_less_than_today(self, mock_get):
        mock_get.return_value = Mock()
        mock_request = self.factory.get('/')
        pk = 'algumId'

        self.green.end_date = date.today() - timedelta(days=1)
        self.viewset._get_tariffs = lambda x, y: (self.blue, self.green)

        mock_get.return_value.get_energy_bills_for_recommendation.return_value = []
        mock_get.return_value.get_energy_bills_pending.return_value = []

        response = self.viewset.retrieve(mock_request, pk)
        self.assertNotIn('Atualize as tarifas vencidas para aumentar a precisão da análise', 'Lance todas as faturas dos últimos 12 meses para aumentar a precisão da análise. Foram lançadas apenas 0 faturas', response.data['warnings'])
